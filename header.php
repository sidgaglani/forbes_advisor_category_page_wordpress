<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/https://www.forbes.com/advisor/t target="_blank"emplate-partials
 *
 * @package WordPress
 * @subpackage Forbes Advisor Category
 * @since 1.0.0
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@400;700&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,400;0,700;1,500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.3.11/tiny-slider.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.3.11/min/tiny-slider.js"></script>
    <?php wp_head(); ?>
    <title>Forbes Advisor Page</title>
  </head>

  <body class="bg-light">
    <nav class="navbar navbar-dark justify-content-between" style="">
      <h1 href="" class="navbar-brand">Forbes</h1>
      <div class="d-flex align-items-center d-none d-lg-block">
        <ul class="navbar-nav mt-0">
          <li class="nav-item">
            <a href="#" target="_blank" class="nav-link">Billionaires</a>
          </li>
          <li class="nav-item">
            <a href="#" target="_blank" class="nav-link">Innovation</a>
          </li>
          <li class="nav-item">
            <a href="#" target="_blank" class="nav-link">Leadership</a>
          </li>
          <li class="nav-item">
            <a href="#" target="_blank" class="nav-link">Money</a>
          </li>
          <li class="nav-item">
            <a href="#" target="_blank" class="nav-link">Consumer</a>
          </li>
          <li class="nav-item">
            <a href="#" target="_blank" class="nav-link">Industry</a>
          </li>
          <li class="nav-item">
            <a href="#" target="_blank" class="nav-link">Lifestyle</a>
          </li>
          <li class="nav-item">
            <a href="#" target="_blank" class="nav-link">Featured</a>
          </li>
          <li class="nav-item">
            <a href="#" target="_blank" class="nav-link">Brandvoice</a>
          </li>
          <li class="nav-item">
            <a href="#" target="_blank" class="nav-link">Advisor</a>
          </li>
        </ul>
      </div>
      <ul class="navbar-nav mt-0">
        <li>
          <a href="">
            <i class="uil uil-search"></i>
          </a>
        </li>
      </ul>
    </nav>