<?php

  function add_theme_scripts() {
    wp_enqueue_style( 'style', get_template_directory_uri() . './assets/css/style.css');
    wp_register_script('app', get_template_directory_uri() . './assets/js/app.js');

    wp_enqueue_script('app');
  }
  add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );


?>
