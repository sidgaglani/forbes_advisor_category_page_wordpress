  <footer>
    <section class="py-5 bg-dark-gray footer-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-lg-12">
            <div class="d-flex justify-content-between align-items-center border-bottom" style="display: none;">
              <div class="navbar-brand text-black">
                <h3>Forbes</h3>
              </div>
              <div>
                <h3 class="font-worksans">Business</h3>
              </div>
              <div>
                <h3 class="font-worksans">Investing</h3>
              </div>
              <div>
                <h3 class="font-worksans">Technology</h3>
              </div>
              <div>
                <h3 class="font-worksans">Entrepreneurs</h3>
              </div>
              <div>
                <h3 class="font-worksans">Op/Ed</h3>
              </div>
              <div>
                <h3 class="font-worksans">Leadership</h3>
              </div>
              <div>
                <h3 class="font-worksans">Lifestyle</h3>
              </div>
              <div>
                <h3 class="font-worksans">Lists</h3>
              </div>
            </div>
            <div class="row footer-subsection mb-5">
              <div class="col-xs-6 col-sm-4 col-lg-2">
                <h3>Conferences</h3>
                <ul>
                  <li>2018 under 30 global</li>
                  <li>2018 under 30 global</li>
                  <li>2018 under 30 global</li>
                  <li>2018 under 30 global</li>
                  <li>2018 under 30 global</li>
                  <li>2018 under 30 global</li>
                  <li>2018 under 30 global</li>
                  <li>2018 under 30 global</li>
                  <li>2018 under 30 global</li>
                  <li>2018 under 30 global</li>
                </ul>
                <h3>Newsletters</h3>
                <ul>
                  <li>Forbes Investor</li>
                  <li>Forbes Investor</li>
                  <li>Forbes Investor</li>
                  <li>Forbes Investor</li>
                  <li>Forbes Investor</li>
                  <li>Forbes Investor</li>
                </ul>
              </div>
              <div class="col-xs-6 col-sm-4 col-lg-3">
                <h3>Products</h3>
                <ul>
                  <li>Forbes Magazine</li>
                  <li>Forbes Magazine</li>
                  <li>Forbes Magazine</li>
                  <li>Forbes Magazine</li>
                  <li>Forbes Magazine</li>
                  <li>Forbes Magazine</li>
                  <li>Forbes Magazine</li>
                  <li>Forbes Magazine</li>
                  <li>Forbes Magazine</li>
                  <li>Forbes Magazine</li>
                </ul>
                <h3>Company Info</h3>
                <ul>
                  <li>Advertise</li>
                  <li>Forbes Careers</li>
                  <li>Forbes Careers</li>
                  <li>Forbes Press room</li>
                  <li>Forbes Press room</li>
                  <li>Advertise</li>
                  <li>Advertise</li>
                </ul>
              </div>
              <div class="col-xs-12 col-sm-4 col-lg-3">
                <h3>Forbes Councils</h3>
                <ul>
                  <li>Forbes Technology Council</li>
                  <li>Forbes Technology Council</li>
                  <li>Forbes Technology Council</li>
                  <li>Forbes Technology Council</li>
                  <li>Forbes Technology Council</li>
                  <li>Forbes Technology Council</li>
                  <li>Forbes Technology Council</li>
                  <li>Forbes Technology Council</li>
                  <li>Forbes Technology Council</li>
                  <li>Forbes Technology Council</li>
                </ul>
                <h3>Education</h3>
                <ul>
                  <li>Forbes School of Business & Technology at Ashford University</li>
                </ul>
              </div>
              <div class="col-xs-12 col-lg-4">
                <div class="row py-4 d-flex justify-content-center">
                  <div class="col-xs-4 col-lg-4">
                    <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/footer-img-1.png ?>" class="img-fluid" alt="">
                  </div>
                  <div class="col-xs-4 col-lg-4">
                    <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/footer-img-2.png ?>" class="img-fluid" alt="">
                  </div>
                  <div class="col-xs-4 col-lg-4">
                    <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/footer-img-3.png ?>" class="img-fluid" alt="">
                  </div>
                </div>
                <div class="row d-flex justify-content-center border-bottom">
                  <div class="col-xs-12 col-lg-12 py-3">
                    <span>2 free issues &nbsp; | &nbsp;</span>
                    <span>Subscriber Services &nbsp; | &nbsp;</span>
                    <span>Gift Subscription</span>                      
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-4 col-lg-4">
                    <ul>
                      <li>Advertise</li>
                      <li>Forbes Careers</li>
                      <li>Forbes Careers</li>
                      <li>Forbes Press room</li>
                      <li>Forbes Press room</li>
                      <li>Advertise</li>
                      <li>Advertise</li>
                    </ul>
                  </div>
                  <div class="col-xs-4 col-lg-4">
                    <ul>
                      <li>Advertise</li>
                      <li>Forbes Careers</li>
                      <li>Forbes Careers</li>
                      <li>Forbes Press room</li>
                      <li>Forbes Press room</li>
                      <li>Advertise</li>
                      <li>Advertise</li>
                    </ul>
                  </div>
                  <div class="col-xs-4 col-lg-4 py-3">
                    <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/footer-img-4.png ?>" class="img-fluid" alt="">
                  </div>
                </div>
              </div>
            </div>
            <div class="d-flex justify-content-between">
              <p class="content">2018 Forbes Media LLC. All rights reserved</p>
              <p class="content">Terms and conditions &nbsp; | &nbsp; Privacy statement &nbsp; | &nbsp; AdChoices</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  </footer>
  </body>
</html>