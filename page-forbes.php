<?php
/**
*
* @package WordPress
* @subpackage Forbes Advisor Category
* @since 1.0.0
* Template name: Forbes
* Template post type: page
*
*/

get_header();
?>

  <section>
    <div class="bg-white py-3 shadow mb-4">
      <div class="container">
        <div class="">
          <div class="col-xs-12 col-lg-2 mb-4">
            <span class="text-uppercase text-tiny">Category</span>
          </div>
          <div class="my-slider">
            <div class="">
              <img srcset="<?php bloginfo('stylesheet_directory');?>/assets/svgs/briefcase.svg" class="" alt="">
              <a>Rewards</a>
            </div>
            <div class="">
              <img srcset="<?php bloginfo('stylesheet_directory');?>/assets/svgs/briefcase.svg" class="" alt="">
              <a>Business</a>
            </div>
            <div class="">
              <img srcset="<?php bloginfo('stylesheet_directory');?>/assets/svgs/aeroplane.svg" class="" alt="">
              <a>Travel & Airline</a>
            </div>
            <div class="">
              <img srcset="<?php bloginfo('stylesheet_directory');?>/assets/svgs/popcorn.svg" class="" alt="">
              <a>Entertainment</a>
            </div>
            <div class="">
              <img srcset="<?php bloginfo('stylesheet_directory');?>/assets/svgs/lock.svg" class="" alt="">
              <a>Bad Credit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="row mb-5">
        <div class="col-xs-12" style="border-bottom: 2px solid #5a71ff;">
          <div class="category-header d-flex align-items-center flex-wrap">
            <i class="uil uil-arrow-left"></i>
            <h3 class="font-weight-normal">&nbsp;Credit Cards</h3>
            <span> &nbsp;|&nbsp; </span>
            <h3>Travel & Airline</h3>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="mb-5">
    <div class="container">
      <div class="row blue-border-bottom">
        <div class="col-xs-12">
          <div class="row">
            <div class="col-xs-12 col-sm-7 col-lg-9 order-1 order-sm-0">
              <div class="d-flex align-items-center">
                <span>
                  <img src="<?php bloginfo('stylesheet_directory');?>/assets/svgs/aeroplane.svg ?>" class="mr-3" alt="">
                </span>
                <h3 class="title">All Travel & Airlines Credit Cards</h4>
                </div>
                <div class="">
                  <p class="subtext mb-5">
                    Planning for a business or personal trip? Don't forget to pack the most important thing in your travel kit - A travel rewards credit card. Wherever your travels may bring you, your card is by your side. Count on us to help you find the card from our partners with the sweetest deals.
                  </p>
                </div>
              </div>
              <div class="col-xs-12 col-sm-5 col-lg-3 order-0 order-sm-1">
                <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/aeroplane.png ?>" class="img-fluid" alt="">
              </div>
            </div>
          </div>
      </div>
    </div>
  </section>
  <section class="mb-5">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="bg-white p-2 shadow rounded">
            <span class="text-uppercase section-heading">Featured</span>
            <h4 class="category-header pb-3 border-bottom">Business Platinum Card®  from American Express</h4>
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-lg-4 order-sm-0 order-lg-0">
                <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/american-express.png ?>" class="mb-4 img-fluid" alt="">
                <button class="btn btn-primary">Apply now</button>
                <p class="text-smallest">On American Express Secure Website</p>
                <button class="btn btn-outline-secondary">Compare</button>
              </div>

              <div class="col-xs-12 col-sm-12 col-lg-5 border-right d-flex flex-column justify-content-between order-sm-1 order-lg-0">
                <p class="content">$150 statement credit after you spend $1,000 in purchases on your new Card within the first 3 months.</p>
                <p class="content">3% cash back at U.S. supermarkets (on up to $6,000 per year in purchases, then 1%)</p>
                <p class="content">Low intro APR: 0% for 15 months on purchases and balance transfers, then a variable rate, currently 14.99% to 25.99%</p>
                <p class="content">2% cash back at U.S. gas stations and at select U.S. department stores, 1% back on other purchases.</p>
                <span class="text-uppercase section-heading font-weight-medium font-worksans">More info</span>
              </div>

              <div class="col-xs-12 col-sm-6 col-lg-3 d-flex flex-column justify-content-between order-sm-0 order-lg-0">
                <div>
                  <span class="text-uppercase text-attributes">Intro apr</span>
                  <h3 class="font-worksans mb-0">2.5%</h3>
                </div>
                <div>
                  <span class="text-uppercase text-attributes">REGULAR APR</span>
                  <h3 class="font-worksans mb-0">16.99-19.99%</h3>
                </div>
                <div>
                  <span class="text-uppercase text-attributes">Annual Fees</span>
                  <h3 class="font-worksans mb-0">$ 550</h3>
                </div>
                <div>
                  <span class="text-uppercase text-attributes">Credit Score</span>
                  <h3 class="font-worksans mb-0">Excellent</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="mb-5">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="bg-white p-2 shadow rounded">
            <!-- <span class="text-uppercase section-heading">Featured</span> -->
            <h4 class="category-header pb-3 border-bottom">Gold Delta SkyMiles Card®  from American Express</h4>
            <div class="row">
              <div class="col-xs-12 col-sm-6 order-0 order-lg-0 col-lg-4">
                <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/skymiles.png ?>" class="mb-4 img-fluid" alt="">
                <button class="btn btn-primary">Apply now</button>
                <p class="text-smallest">On American Express Secure Website</p>
                <button class="btn btn-outline-secondary">Compare</button>
              </div>

              <div class="col-xs-12 col-sm-12 col-lg-5 order-1 order-lg-0 border-right d-flex flex-column justify-content-between">
                <p class="content">$150 statement credit after you spend $1,000 in purchases on your new Card within the first 3 months.</p>
                <p class="content">3% cash back at U.S. supermarkets (on up to $6,000 per year in purchases, then 1%)</p>
                <p class="content">Low intro APR: 0% for 15 months on purchases and balance transfers, then a variable rate, currently 14.99% to 25.99%</p>
                <p class="content">2% cash back at U.S. gas stations and at select U.S. department stores, 1% back on other purchases.</p>
                <span class="text-uppercase section-heading font-weight-medium font-worksans">More info</span>
              </div>

              <div class="col-xs-12 col-sm-6  order-0 order-lg-0 col-lg-3 d-flex flex-column justify-content-between">
                <div>
                  <span class="text-uppercase text-attributes">Intro apr</span>
                  <h3 class="font-worksans mb-0">2.5%</h3>
                </div>
                <div>
                  <span class="text-uppercase text-attributes">REGULAR APR</span>
                  <h3 class="font-worksans mb-0">16.99-19.99%</h3>
                </div>
                <div>
                  <span class="text-uppercase text-attributes">Annual Fees</span>
                  <h3 class="font-worksans mb-0">$ 550</h3>
                </div>
                <div>
                  <span class="text-uppercase text-attributes">Credit Score</span>
                  <h3 class="font-worksans mb-0">Excellent</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="mb-5">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="bg-white p-2 shadow rounded">
            <!-- <span class="text-uppercase section-heading">Featured</span> -->
            <h4 class="category-header pb-3 border-bottom">Simply Cash Card®  from American Express</h4>
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-lg-4 order-sm-0 order-lg-0">
                <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/american-business.png ?>" class="mb-4 img-fluid" alt="">
                <button class="btn btn-primary">Apply now</button>
                <p class="text-smallest">On American Express Secure Website</p>
                <button class="btn btn-outline-secondary">Compare</button>
              </div>

              <div class="col-xs-12 col-lg-5 border-right d-flex flex-column justify-content-between order-sm-1 order-lg-0">
                <p class="content">$150 statement credit after you spend $1,000 in purchases on your new Card within the first 3 months.</p>
                <p class="content">3% cash back at U.S. supermarkets (on up to $6,000 per year in purchases, then 1%)</p>
                <p class="content">Low intro APR: 0% for 15 months on purchases and balance transfers, then a variable rate, currently 14.99% to 25.99%</p>
                <p class="content">2% cash back at U.S. gas stations and at select U.S. department stores, 1% back on other purchases.</p>
                <span class="text-uppercase section-heading font-weight-medium font-worksans">More info</span>
              </div>

              <div class="col-xs-12 col-sm-6 col-lg-3 d-flex flex-column justify-content-between order-sm-0 order-lg-0">
                <div>
                  <span class="text-uppercase text-attributes">Intro apr</span>
                  <h3 class="font-worksans mb-0">2.5%</h3>
                </div>
                <div>
                  <span class="text-uppercase text-attributes">REGULAR APR</span>
                  <h3 class="font-worksans mb-0">16.99-19.99%</h3>
                </div>
                <div>
                  <span class="text-uppercase text-attributes">Annual Fees</span>
                  <h3 class="font-worksans mb-0">$ 550</h3>
                </div>
                <div>
                  <span class="text-uppercase text-attributes">Credit Score</span>
                  <h3 class="font-worksans mb-0">Excellent</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="mb-5 d-none" id="load-more-content">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="bg-white p-2 shadow rounded">
            <!-- <span class="text-uppercase section-heading">Featured</span> -->
            <h4 class="category-header pb-3 border-bottom">Test card</h4>
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-lg-4 order-sm-0 order-lg-0">
                <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/american-business.png ?>" class="mb-4 img-fluid" alt="">
                <button class="btn btn-primary">Apply now</button>
                <p class="text-smallest">On American Express Secure Website</p>
                <button class="btn btn-outline-secondary">Compare</button>
              </div>

              <div class="col-xs-12 col-lg-5 border-right d-flex flex-column justify-content-between order-sm-1 order-lg-0">
                <p class="content">$150 statement credit after you spend $1,000 in purchases on your new Card within the first 3 months.</p>
                <p class="content">3% cash back at U.S. supermarkets (on up to $6,000 per year in purchases, then 1%)</p>
                <p class="content">Low intro APR: 0% for 15 months on purchases and balance transfers, then a variable rate, currently 14.99% to 25.99%</p>
                <p class="content">2% cash back at U.S. gas stations and at select U.S. department stores, 1% back on other purchases.</p>
                <span class="text-uppercase section-heading font-weight-medium font-worksans">More info</span>
              </div>

              <div class="col-xs-12 col-sm-6 col-lg-3 d-flex flex-column justify-content-between order-sm-0 order-lg-0">
                <div>
                  <span class="text-uppercase text-attributes">Intro apr</span>
                  <h3 class="font-worksans mb-0">2.5%</h3>
                </div>
                <div>
                  <span class="text-uppercase text-attributes">REGULAR APR</span>
                  <h3 class="font-worksans mb-0">16.99-19.99%</h3>
                </div>
                <div>
                  <span class="text-uppercase text-attributes">Annual Fees</span>
                  <h3 class="font-worksans mb-0">$ 550</h3>
                </div>
                <div>
                  <span class="text-uppercase text-attributes">Credit Score</span>
                  <h3 class="font-worksans mb-0">Excellent</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="" id="load-more-button">
    <a href="javascript:void(0)">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 pb-5" style="border-bottom:4px solid #5a71ff">
            <div class="bg-gray py-3">
              <h3 class="font-worksans text-uppercase text-black" style="font-size: 15px;text-align: center;">show more cards</h3>
            </div>
          </div>
        </div>
      </div>
    </a>
  </section>

  <div class="mb-5">
    <div class="container">
      <div class="row">
        <div class="col-xs-12" style="border-bottom:1px solid #5a71ff">
          <h3 class="text-blue">Credit Cards&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Forbes Editor’s Picks</h3>
        </div>
      </div>
    </div>
  </div>
  <section>
    <div class="container">
      <div class="row mb-5">
        <div class="col-xs-12 border-bottom">
          <div class="row">
            <div class="col-xs-12 col-lg-8 mb-4">
              <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/large-img.png ?>" class="img-fluid" alt="">
              <div class="row d-flex mb-4  border-bottom">
                <div class="col-xs-12 col-lg-10 pb-3" style="margin-left: 12.5%; margin-right: 12.5%;">
                  <h4 class="text-center" style="font-size: 26px;">Personal finance advisor from those who know money best</h4>
                  <p class="content text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                </div>
              </div>
              <div class="row pb-3">
                <div class="col-xs-12 col-sm-6 col-lg-6 border-right">
                  <div class="row mb-4">
                    <div class="col-xs-3  col-lg-3">
                      <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/small-img-1.png ?>" class="img-fluid" alt="">
                    </div>
                    <div class="col-xs-9  col-lg-9">
                      <p>Personal finance advisor from those who know money best</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-3  col-lg-3">
                      <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/small-img-2.png ?>" class="img-fluid" alt="">
                    </div>
                    <div class="col-xs-9  col-lg-9">
                      <p>Personal finance advisor from those who know money best</p>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-6">
                  <div class="row mb-4">
                    <div class="col-xs-3 col-lg-3">
                      <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/small-img-1.png ?>" class="img-fluid" alt="">
                    </div>
                    <div class="col-xs-9 col-lg-9">
                      <p>Personal finance advisor from those who know money best</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-3 col-lg-3">
                      <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/small-img-2.png ?>" class="img-fluid" alt="">
                    </div>
                    <div class="col-xs-9 col-lg-9">
                      <p>Personal finance advisor from those who know money best</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-4 d-flex flex-column justify-content-around d-sm-none d-lg-block">
              <div class="mb-4">
                <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/medium-img.png ?>" class="img-fluid mb-2" alt="">
                <span class="font-worksans">Forbes List</span>
                <h4>Personal finance advisor from those who know money best</h6>
                <p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
              <div class="mb-4">
                <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/medium-img-2.png ?>" class="img-fluid mb-2" alt="">
                <span class="font-worksans" style="color:#887412;">Billionaires</span>
                <h4>Personal finance advisor from those who know money best</h6>
                <p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="row mb-5">
        <div class="col-xs-12 col-lg-12">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-lg-4 pb-3 border-bottom">
              <div>
                <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/medium-img-5.png ?>" class="mb-3 img-fluid" alt="">
                <h3 style="font-size: 16px;">Personal finance advisor from those who know money best</h3>
                <span class="text-tiny font-weight-bold">By User Name</span>
                <span class="text-tiny text-gray">&nbsp;6 min read</span>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-4 pb-3 border-bottom">
              <div>
                <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/medium-img-6.png ?>" class="mb-3 img-fluid" alt="">
                <h3 style="font-size: 16px;">Personal finance advisor from those who know money best</h3>
                <span class="text-tiny font-weight-bold">By User Name</span>
                <span class="text-tiny text-gray">&nbsp;6 min read</span>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-4 pb-3 border-bottom">
              <div>
                <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/medium-img-3.png ?>" class="mb-3 img-fluid" alt="">
                <h3 style="font-size: 16px;">Personal finance advisor from those who know money best</h3>
                <span class="text-tiny font-weight-bold">By User Name</span>
                <span class="text-tiny text-gray">&nbsp;6 min read</span>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-4 pb-3 border-bottom">
              <div>
                <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/medium-img-6.png ?>" class="mb-3 img-fluid" alt="">
                <h3 style="font-size: 16px;">Personal finance advisor from those who know money best</h3>
                <span class="text-tiny font-weight-bold">By User Name</span>
                <span class="text-tiny text-gray">&nbsp;6 min read</span>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-4 pb-3 border-bottom">
              <div>
                <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/medium-img-3.png ?>" class="mb-3 img-fluid" alt="">
                <h3 style="font-size: 16px;">Personal finance advisor from those who know money best</h3>
                <span class="text-tiny font-weight-bold">By User Name</span>
                <span class="text-tiny text-gray">&nbsp;6 min read</span>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-4 pb-3 border-bottom">
              <div>
                <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/medium-img-5.png ?>" class="mb-3 img-fluid" alt="">
                <h3 style="font-size: 16px;">Personal finance advisor from those who know money best</h3>
                <span class="text-tiny font-weight-bold">By User Name</span>
                <span class="text-tiny text-gray">&nbsp;6 min read</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="row mb-5">
        <div class="col-xs-12 col-lg-12">
          <div class="mb-4 blue-border-bottom">
            <h3 class="font-worksans text-blue">Credit Cards</h3>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-4 col-lg-4 order-1 order-lg-0">
              <div>
                <h2 class="mb-4 mt-0">Try Forbes Advisor for Credit Cards</h3>
                <p class="subtext">Compare Cards to find one that suits your financial priorities</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-lg-4 d-flex align-items-center justify-content-center order-2 order-lg-1">
              <button class="btn btn-primary" style="width: 75%;">Know more <span><i class="uil uil-arrow-right"></i></span></button>
            </div>
            <div class="col-xs-12 col-sm-4 col-lg-4 order-0 order-lg-2">
              <img src="<?php bloginfo('stylesheet_directory');?>/assets/images/visa-card.png ?>" class="img-fluid" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <script defer>
    const slider = tns({
      container: '.my-slider',
      loop: true,
      slideBy: 'page',
      nav: false,  
      autoplay: true,
      speed: 400,
      autoplayButtonOutput: false,
      mouseDrag: true,
      lazyload: true,
      responsive: {
        300: {
          items: 1,
        },        
        768: {
          items: 3,
        }
      }
    
    });
  </script>

  <script defer>
    var loadMoreButton = document.getElementById('load-more-button');
    var loadMoreContent = document.getElementById('load-more-content')
    loadMoreButton.addEventListener('click', function(){
      if(loadMoreContent.classList.contains('d-none')){
        loadMoreContent.classList.remove('d-none');
        loadMoreContent.classList.add('d-block');
        this.classList.add('d-none')
      }
    })
  </script>


  <?php get_footer(); ?>

